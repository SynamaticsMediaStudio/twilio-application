# Bitechat Front-End

## Introduction

Bitechat is a Laravel and Vue.js based project that provides a chat room experience for users. It includes a chat room interface where users can communicate with each other in real-time.

## Installation

To install the project, follow these steps:

1. Clone the repository
2. Install the dependencies using npm
3. Configure the Laravel project
4. Run the Laravel server
5. Open the Vue.js interface in your browser

## Technologies Used

- Laravel 5.8 for the backend: a PHP framework that provides a robust and scalable foundation for building web applications.
- Vue.js 2.6 for the frontend: a JavaScript framework that provides a clear and concise syntax for building user interfaces.
- Socket.io for real-time communication: a JavaScript library that enables real-time, bidirectional communication between a client and a server.
- Twilio for SMS notifications: a cloud communications platform that provides a simple API for sending and receiving SMS messages.

## Features

- Users can create a chat room and invite other users to join
- Users can chat in real-time with other users in the chat room
- Users can receive SMS notifications when they are invited to a chat room
- Users can view a list of active chat rooms and join the one that interests them

## Getting Started

To get started with this project, follow these steps:

1. Clone the repository
2. Install the dependencies using npm
3. Rename the `.env.example` file to `.env` and configure the necessary settings
4. Run the `php artisan key:generate` command to generate an application key
5. Run the `php artisan migrate` command to create the necessary database tables
6. Run the `php artisan serve` command to start the Laravel server
7. Open the Vue.js interface in your browser at http://localhost:8000

## Contributing

Contributions are welcome! If you find any issues or have suggestions for improvement, please submit a pull request or open an issue on the GitHub repository.

