<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function chats()
    {
        return $this->hasMany('App\Message');
    }
}
