<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $customers = Customer::with(['chats'])->get();
        return $customers;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "number"=>"required|string|unique:customers",
        ]);
        $customer = new Customer;
        $customer->number       = $request->number;
        $customer->name         = ($request->name)?$request->name:"";
        $customer->email        = ($request->email)?$request->email:"";
        $customer->restaurant   = ($request->restaurent)?$request->restaurent:"";
        $customer->location     = ($request->location)?$request->location:"";
        $customer->save();
        return response()->json(["success"],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $customer->name         = ($request->name)?$request->name:"";
        $customer->email        = ($request->email)?$request->email:"";
        $customer->restaurant   = ($request->restaurent)?$request->restaurent:"";
        $customer->location     = ($request->location)?$request->location:"";
        $customer->save();
        return response()->json(["success"],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $customer->status = 0;
        $customer->save();
        $chat = new \App\Message;
        $chat->customer_id = $customer->id;
        $chat->author = 'user';
        $chat->message = "Chat Closed";
        $chat->twilio_message_id = null;
        $chat->media = null;
        $chat->status = 0;
        $chat->save();
        return response()->json(["success"],200);        
    }
}
