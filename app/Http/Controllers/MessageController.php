<?php

namespace App\Http\Controllers;

use App\Message;
use App\Notifications\NewMessage;
use Illuminate\Http\Request;
use Twilio\Rest\Client as TwilioClient;
use Pusher\Pusher;
class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }
    public function Incoming(Request $request)
    {
        $user = \App\User::find(1);
        $customer = \App\Customer::where('number',$request->From)->first();
        if(!$customer){
            $isnew = true;
            $customer= new \App\Customer;
            $customer->number = $request->From;
        }
        if($customer->status = 0){
            $ms1                    = new Message;
            $ms1->message           = "Chat Reopened";
            $ms1->twilio_message_id = null;
            $ms1->media             = null;
            $ms1->customer_id       = $customer->id;
            $ms1->author            = "user";
            $ms1->status            = 0;
            $ms1->save();
        }
        $customer->status = 1;
        // $customer->save();

        $message                    = new Message;
        $message->message           = ($request->Body) ? $request->Body:"";
        $message->twilio_message_id = $request->SmsMessageSid;
        $message->media             = ($request->MediaUrl0) ? $request->MediaUrl0:null;
        $message->customer_id       = $customer->id;
        $message->author            = "customer";
        $message->status            = 1;
        // $message->save();
        
        $options = array(
            'cluster' => 'ap2',
            'useTLS' => true
          );
          $pusher = new Pusher(
            '5810d0da23209a6b048e',
            'd8f470745c1c96c45665',
            '427290',
            $options
          );
          $response = $pusher->get( '/channels/my-channel' );
            if( $response[ 'status'] == 200 ) {
                $users = json_decode( $response[ 'body' ], true );
            }
            if($response){
                $res = $pusher->trigger('my-channel', 'incoming', $message);
            }
            else{
                $sid        = "AC23c1ce6c98db5290bb4dc1a1afe6d4b1";
                $token      = "5d107b7d3d1e27fa6fbc0f07dbbb6dc8";        
                $twilio = new TwilioClient( $sid, $token );
                $message = [];
                $message['from'] = "+1 469 405 2434";
                $message['body'] = "Message from ".$message->message->customer->name.":".$message->message->message;
                if($message->message->media){
                    $message['mediaUrl'] = $message->message->media;
                }
                $send   = $twilio->messages->create($user->mobile_number, $message);
            }
        return response()->json($res,200);



    }

    public function Email(Request $request)
    {
        $user = \App\User::find(1);
        $msg = Message::find($request->message);
        $sid        = "AC23c1ce6c98db5290bb4dc1a1afe6d4b1";
        $token      = "5d107b7d3d1e27fa6fbc0f07dbbb6dc8";        
        $twilio = new TwilioClient( $sid, $token );
        $message = [];
        $message['from'] = "+1 469 405 2434";
        $message['body'] = "Message from ".$msg->message->customer->name.":".$msg->message->message;
        if($msg->message->media){
            $message['mediaUrl'] = $msg->message->media;
        }
        $send   = $twilio->messages->create($user->mobile_number, $message);

        return "OK";
    }

    public function Send(Request $request)
    {
        // return dd($request->file('image'));
        $request->validate([
            "id"=>"required|string|exists:customers",
            "text"=>"nullable|string",
            "image"=>"nullable|file",
        ]);
        $customer   = \App\Customer::find($request->id);
        $sid        = "AC23c1ce6c98db5290bb4dc1a1afe6d4b1";
        $token      = "5d107b7d3d1e27fa6fbc0f07dbbb6dc8";
        $twilio = new TwilioClient( $sid, $token );
        $message = [];
        $message['from'] = "+1 469 405 2434";
        $message['body'] = ($request->text) ? $request->text:"";
        if($request->hasFile('image')){
            $image = $request->file('image')->store('sms','public');
            
            // $message['mediaUrl'] ="https://mybiteheist.app/root/storage/app/".$image;
            // $message['mediaUrl'] = "https://i.ytimg.com/vi/lM3jtRAwJlM/maxresdefault.jpg";
        }
        $send   = $twilio->messages->create($customer->number, $message);
        $mLine                    = new Message;
        $mLine->message           = (isset($message['body']))? $message['body']:"";
        $mLine->twilio_message_id = $send->sid;
        $mLine->media             = (isset($message['mediaUrl'])) ? $message['mediaUrl']:null;
        $mLine->customer_id       = $customer->id;
        $mLine->author            = "user";
        $mLine->status            = 0;
        $mLine->save();
        $messread                   = Message::where('customer_id',$customer->id)->update(['status'=>0]);
        return response()->json("success",200);

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }
}
