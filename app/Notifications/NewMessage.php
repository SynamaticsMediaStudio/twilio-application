<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Twilio\Rest\Client as TwilioClient;

class NewMessage extends Notification
{
    use Queueable;
    protected $message;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(object $message) {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $sid        = "AC23c1ce6c98db5290bb4dc1a1afe6d4b1";
        $token      = "5d107b7d3d1e27fa6fbc0f07dbbb6dc8";        
        $twilio = new TwilioClient( $sid, $token );
        $message = [];
        $message['from'] = "+1 469 405 2434";
        $message['body'] = "Message from ".$this->message->customer->name.":".$this->message->message;
        if($this->message->media){
            $message['mediaUrl'] = $this->message->media;
        }

        $send   = $twilio->messages->create($notifiable->mobile_number, $message);

        return (new MailMessage)
                    ->subject('You have a new message from '.$this->message->customer->name)
                    ->line('Hi '.$notifiable->name)
                    ->line('You have a new message from '.$this->message->customer->name)
                    ->line("Here is the message:")
                    ->line($this->message->message)
                    ->line($this->message->media)
                    ->action('View Message', url('/'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
