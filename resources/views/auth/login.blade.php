@extends('layouts.app')

@section('content')
<div class="login-body">
        <div class="container">
          <form class="form-signin" method="POST" action="{{ route('login') }}">
                @csrf
            <h2 class="form-signin-heading">sign in now</h2>
            <div class="login-wrap">
                <div class="form-group">
                    <input id="email" placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input placeholder="Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                <label class="checkbox">
                    <input type="checkbox"  type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Remember me
                    <span class="pull-right">
                        <a href="{{ route('password.request') }}"> Forgot Password?</a>
    
                    </span>
                </label>
                <button class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>
                <div class="registration">
                    Don't have an account yet?
                    <a class="" href="{{route('register')}}">
                        Create an account
                    </a>
                </div>
            </div>
          </form>
        </div>
    </div>
@endsection
