@extends('layouts.app')

@section('content')
<div class="login-body">
    <div class="container">
    <form class="form-signin" method="POST" action="{{ route('register') }}">
            @csrf
        <h2 class="form-signin-heading">Register</h2>
        <div class="login-wrap">
            <div class="form-group">
                <input id="name" placeholder="Name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                @error('name')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <input id="mobile_number" placeholder="Mobile No" type="text" class="form-control @error('mobile_number') is-invalid @enderror" name="mobile_number" value="{{ old('mobile_number') }}" required autocomplete="phone">
                <small>This Number will be used to send Notifications when you are offline</small>
                @error('mobile_number')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <input id="email" placeholder="Email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required>
                <small>This Email will be used to send Notifications when you are offline</small>
                @error('email')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required >
                @error('password')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control @error('password-confirm') is-invalid @enderror" name="password-confirm" value="{{ old('password-confirm') }}" required>
            </div>


            <button class="btn btn-lg btn-login btn-block" type="submit">Register</button>
            <div class="registration">
                Don't have an account yet?
                <a class="" href="{{route('login')}}">
                    Already have an account?
                </a>
            </div>
        </div>
        </form>        
    </div>
</div>
@endsection
