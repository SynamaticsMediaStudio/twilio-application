<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="{{asset("frontend/css/bootstrap.min.css")}}" rel="stylesheet"/>
    <link href="{{asset("frontend/css/bootstrap-reset.css")}}" rel="stylesheet"/>
    <link href="{{asset("frontend/assets/font-awesome/css/font-awesome.css")}}" rel="stylesheet" />
    <link href="{{asset("frontend/css/style.css")}}" rel="stylesheet"/>
    <link href="{{asset("frontend/css/style-responsive.css")}}" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="{{asset("frontend/js/html5shiv.js")}}"></script>
      <script src="{{asset("frontend/js/respond.min.js")}}"></script>
    <![endif]-->
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <section id="container" class="">
        @auth
            @include('layouts.auth')
        @else
            @include('layouts.guest')
        @endauth
    </section>
</div>

<script src="{{asset("frontend/js/jquery.js")}}"></script>
<script src="{{asset("frontend/js/bootstrap.min.js")}}"></script>
<script src="{{asset("frontend/js/jquery.dcjqaccordion.2.7.js")}}" class="include" type="text/javascript"></script>
<script src="{{asset("frontend/js/jquery.scrollTo.min.js")}}"></script>
<script src="{{asset("frontend/js/jquery.nicescroll.js")}}" type="text/javascript"></script>
<script src="{{asset("frontend/js/respond.min.js")}}" ></script>
<script src="{{asset("frontend/js/common-scripts.js")}}"></script>    
</body>
</html>
