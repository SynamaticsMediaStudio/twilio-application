        <!--header start-->
        <header class="header white-bg">
            <div class="sidebar-toggle-box">
                <div data-original-title="Toggle Navigation" data-placement="right" class="fa fa-bars tooltips"></div>
            </div>
            <!--logo start-->
            <a href="/" class="logo" >SERVICE</a>
            <!--logo end-->
            <form id="logout-form" class="pull-right py-3" action="{{ route('logout') }}" method="POST" >
                @csrf
                <button type="submit" class="btn btn-link">Logout</button>
            </form>            
        </header>
        <!--header end-->
        <!--sidebar start-->
        <aside>
            <div id="sidebar"  class="nav-collapse ">
                <!-- sidebar menu start-->
                <ul class="sidebar-menu" id="nav-accordion">
                    <li>
                        <a href="/">
                            <i class="fa fa-dashboard"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                </ul>
            </div>
        </aside>
        <!--sidebar end-->
        <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
                <!-- page start-->
                @yield('content')
                <!-- page end-->
            </section>
        </section>