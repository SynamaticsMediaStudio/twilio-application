var cors = require('cors');

var app = require('express')();

var http = require('http').Server(app);

var io = require('socket.io')(http);

app.use(cors({credentials: true, origin: 'http://localhost:8000'}));
app.get('/', function(req, res){
  res.sendFile(__dirname + '/app.html');
});
app.post('/', function(req, res){
  res.sendFile(__dirname + '/app.html');
});

io.on('connection', function(socket){
    console.log('a user connected');
    socket.emit('chat message', "S");
    socket.on('disconnect', function(){
      console.log('user disconnected');
    });
  });
http.listen(3000, function(){
  console.log('listening on *:3000');
});